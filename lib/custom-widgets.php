<?php

namespace Auctioneer;

/**
 * The Widgets
 */
class Widgets
{
    function __construct(Common $common){
        $this->common = $common;

        add_filter('auctioneer-default-widgets', array($this, 'get_widgets'), 1, 10);
        add_action('plugins_loaded', array($this, 'include_widgets'), 15);
        add_action('widgets_init', array($this, 'create_widgets'), 10);
    }

    function get_widgets(){
         $widgets = array(
            'recent_auctions' => array(
                'filepath'  => 'widgets/upcoming-auctions.php',
                'classname' => 'Auctioneer\UpcomingAuctions',
            ),
            'auction_contact' => array(
               'filepath'  => 'widgets/auction-contact.php',
               'classname' => 'Auctioneer\AuctionContact',
            ),
            // 'auction_search' => array(
            //    'filepath'  => 'widgets/auction-search.php',
            //    'classname' => 'Auctioneer\AuctionSearch',
            // ),
            'document_list' => array(
                'filepath'  => 'widgets/document-list.php',
                'classname' => 'Auctioneer\DocumentList',
            ),
        );
        return $widgets;
    }

    function include_widgets(){
        $widgets = apply_filters('auctioneer-default-widgets', array());

        foreach ( $widgets as $widget ){
            require_once($this->common->lib_dir . $widget['filepath']);
        }
    }

    function create_widgets(){
        $widgets = apply_filters('auctioneer-default-widgets', array());

        foreach ( $widgets as $widget ){
            register_widget($widget['classname']);
        }
    }
}
