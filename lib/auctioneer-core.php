<?php

/*
 * The Auctioneer Core Class loads the entire plugin
 */

namespace Auctioneer;
use Auctioneer\Common;
use Auctioneer\Thirdparty;
use Auctioneer\InformationArchitecture;
use Auctioneer\Widgets;
use Auctioneer\Admin;
use Auctioneer\Display;
use Auctioneer\UI;
use Auctioneer\Search;

class Core{

    private $common;

    function __construct(){
        add_action('plugins_loaded', array($this,'load'), 5);
    }

    function load(){
        //commonly used utility stuff like verion, slug, etc. first
        require_once('common.php');
        $common = new Common();

        //load third party resources first so they're available
        require_once('thirdparty.php');
        $thirdparty = new Thirdparty($common);

        require_once('ia.php');
        $ia = new InformationArchitecture($common);

        require_once('custom-widgets.php');
        $widgets = new Widgets($common);

        if(is_admin()){
            require_once($common->lib_dir . 'admin/admin.php');
            new Admin($common);

            require_once($common->lib_dir . 'admin/settings.php');
            new Settings($common);

            require_once($common->lib_dir . 'admin/metaboxes.php');
            new Metaboxes($common);

        }
        else{

            require_once($common->lib_dir . 'display/ui.php');
            new UI($common);

            require_once($common->lib_dir . 'display/filters.php');
            new Filters($common);

            require_once($common->lib_dir . 'display/shortcodes.php');
            new Shortcodes($common);

            require_once($common->lib_dir . 'display/query.php');
            new Query($common);

            require_once($common->lib_dir . 'display/search.php');
            new Search($common);

            require_once($common->lib_dir . 'admin/automation.php');
            new Automation($common);
        }
    }

}
