<?php

namespace Auctioneer;
use Auctioneer\Common;

/**
 * The Auctioneer Shortcodes Class controls things found only on the public facing pages.
 * This class is only loaded when  'is_admin() == false'
 */
class Shortcodes
{
    function __construct(Common $common){
        add_shortcode('auction_properties', array($this, 'display_auction_properties'), 1, 10);
        add_shortcode('auction_date', array($this, 'display_auction_date'), 1, 10);
        add_shortcode('auction_address', array($this, 'display_address'), 1, 10);
        add_shortcode('auction_manager', array($this, 'display_manager'), 1, 10);
        add_shortcode('property_zoning', array($this, 'display_zoning'), 1, 10);
        add_shortcode('property_address', array($this, 'display_address'), 1, 10);
        add_shortcode('property_map', array($this, 'display_map'), 1, 10);
        add_shortcode('property_details', array($this, 'display_property_details'), 1, 10);
    }

    function display_auction_properties($args, $post){
        global $post;
        $post_holder = $post;
        // I had to do the above because wp_reset_postdata(); wasn't working
        // the connected query was jacking up the query/ post object
        if(function_exists('p2p_register_connection_type')){
            $connected = $this->get_related_items('auctioneer_auction_to_property');
            if(!$connected){
                return '';
            }
            $format = (isset($args['format'])) ? $args['format'] : 'csv';
            switch($format){
                default:
                case 'csv':
                    $list = apply_filters('auctioneer-related-csv', $connected);
                break;
                case 'list':
                    $list = apply_filters('auctioneer-related-list', $connected);
            }
            $post = $post_holder;
            return apply_filters('auctioneer-property-list', $list) ;
        }else{
            return '<!--no properties connected-->';
        }
    }

    function get_related_items($type, $args = array()){
        // Find connected *$type*
        $connected = new \WP_Query( array(
          'connected_type' => $type,
          'connected_items' => get_queried_object(),
          'nopaging' => true,
        ) );
        if($connected->have_posts()){
            return $connected;
        }else{
            return false;
        }
    }


    function display_auction_date($args = array()){
        $date_string = '';
        $date_string .= (!is_array($args)) ? sprintf('%s<span class="date-seperator"> %s </span>%s',
            apply_filters('auctioneer-nice-date', get_post_meta(get_the_id(), '_auction_start', true)),
            __('through', 'auctioneer'),
            apply_filters('auctioneer_nice_date', get_post_meta(get_the_id(), '_auction_end', true))) : '';
        $date_string .= (is_array($args) && in_array('start', $args)) ? apply_filters('auctioneer-nice-date', get_post_meta(get_the_id(), '_auction_start', true)) : '';
        $date_string .= (is_array($args) && in_array('end', $args)) ? apply_filters('auctioneer-nice-date', get_post_meta(get_the_id(), '_auction_end', true)) : '';
        return apply_filters('auction-date-formatting', $date_string);
    }

    function display_address($args, $post_id, $code){
        if(!in_array(get_post_type(), array('auctioneer_auction', 'auctioneer_property'))){
            return '';
        }
        $custom = get_post_custom( get_the_id() );
        if(get_post_type() == 'auctioneer_auction'){
            $meta_prefix = '_auction';
        }elseif(get_post_type() == 'auctioneer_property' ){
            $meta_prefix = '_property';
        }

        $address_parts = array(
            'address_1' => (isset($custom[$meta_prefix . '_address_1'])) ?$custom[$meta_prefix . '_address_1'][0] : false ,
            'address_2' => (isset($custom[$meta_prefix . '_address_2'])) ?$custom[$meta_prefix . '_address_2'][0] : false ,
            'city' => (isset($custom[$meta_prefix . '_address_city'])) ?$custom[$meta_prefix . '_address_city'][0] : false ,
            'state' => (isset($custom[$meta_prefix . '_address_state'])) ?$custom[$meta_prefix . '_address_state'][0] : false ,
            'zip' => (isset($custom[$meta_prefix . '_address_zip'])) ?$custom[$meta_prefix . '_address_zip'][0] : false
        );

        return apply_filters('auctioneer-address-format', '', $address_parts);
    }


    function display_manager($args){
        $users = get_users( array(
          'connected_type' => 'auction_manager',
          'connected_items' => get_the_id()
        ) );

        if(is_array($users)){
            $mgr_html = '';
            foreach($users as $user){
                $mgr_html .= apply_filters('auctioneer-manager-contact', '', $user);
            }
            return $mgr_html;
        }
    }


    function display_zoning($args){
        $types = wp_get_post_terms( get_the_id(), 'auctioneer_zoning' );
        if (!$types){
            return '';
        }

        $counter = $total = count($types);
        $zoning_html = '<span class="property-zoning">';

        foreach($types as $type){
            if ($total == 1 || $counter == $total){
                $zoning_html .=  $type->name;
            }else{
                $zoning_html .= sprintf('/%s', $type->name);
            }
            $counter--;
        }
        return $zoning_html . '</span>';
    }

    function display_map($args){
        $map_url = get_post_meta(get_the_id(), '_map_url', true);
        if(!$map_url){
            return '<!--no map url provided-->';
        }

        if(!isset($args['format']) || $args['format'] == 'link'){
            return sprintf('<a href="%s" class="map-url" target="_blank" title="%s">%s</a>', $map_url, __('Map of ', 'auctioneer') . the_title_attribute('echo=0'), __('View Map', 'auctioneer'));
        }elseif($args['format'] == 'embed'){
            return sprintf('<iframe src="%s" class="map-embed">%s</iframe>', $map_url, __('Enable Iframes to see map', 'auctioneer'));
        }

    }

    function display_property_details($args){
        $custom = get_post_custom(get_the_id());
        if(!$custom){
            return '<!-- no custom data found -->';
        }

        if(isset($args['format']) && $args['format'] == 'table'){
            $markup = '<table class="property-details">';
            $format = '<tr><td class="detail-key">%s</td><td class="detail-value">%s</td></tr>';
        } else {
            $markup = '<ul class="property-details">';
            $format = '<li class="detail"><span class="key">%s: </span><span class="value">%s</span></li>';
        }

        $markup .= (isset($custom['_property_bedrooms'])) ? sprintf($format, __('Bedrooms', 'auctioneer'), apply_filters('auctioneer-bedrooms', $custom['_property_bedrooms'][0])) : '';
        $markup .= (isset($custom['_property_bathrooms'])) ? sprintf($format, __('Bathrooms', 'auctioneer'), apply_filters('auctioneer-bathrooms', $custom['_property_bathrooms'][0])) : '';
        $markup .= (isset($custom['_property_bldg_squarefootage'])) ? sprintf($format, __('Building Sq. Footage', 'auctioneer'), apply_filters('auctioneer-bldg-sqft', $custom['_property_bldg_squarefootage'][0])) : '';
        $markup .= (isset($custom['_property_acreage'])) ? sprintf($format, __('Acreage', 'auctioneer'), apply_filters('auctioneer-acreage', $custom['_property_acreage'][0])) : '';
        $markup .= (isset($custom['_property_lot_sqfootage'])) ? sprintf($format, __('Lot Sq. Footage', 'auctioneer'), apply_filters('auctioneer-lot-sqft', $custom['_property_lot_sqfootage'][0])) : '';
        $markup .= (isset($custom['_property_year_built'])) ? sprintf($format, __('Year Built', 'auctioneer'), apply_filters('auctioneer-year-built', $custom['_property_year_built'][0])) : '';
        $markup .= (isset($custom['_property_parcel_number'])) ? sprintf($format, __('Parcel Number', 'auctioneer'), apply_filters('auctioneer-parcel-num', $custom['_property_parcel_number'][0])) : '';
        $markup .= (isset($custom['_property_tax_value'])) ? sprintf($format, __('Property Tax Value', 'auctioneer'), apply_filters('auctioneer-tax-value', $custom['_property_tax_value'][0])) : '';
        $markup .= (isset($custom['_property_year_taxes'])) ? sprintf($format, __('Last Year Taxes', 'auctioneer'), apply_filters('auctioneer-year-taxes', $custom['_property_year_taxes'][0])) : '';

        if(isset($args['format']) && $args['format'] == 'table'){
            $markup .= '</table>';
        } else {
            $markup .= '</ul>';
        }

        return $markup;
    }

}
