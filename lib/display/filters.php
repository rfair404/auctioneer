<?php
namespace Auctioneer;
use Auctioneer\Common;

class Filters{
    function __construct(Common $common){

        add_filter('post-meta-template', array($this, 'meta_display'), 2, 10);
        add_filter('auctioneer-phone-format', array($this, 'format_phone'), 1, 10);
        add_filter('auctioneer-email-format', array($this, 'format_email'), 1, 10);
        add_filter('auctioneer-mailto-format', array($this, 'format_mailto'), 1, 10);
        add_filter('auctioneer-cc-addresses', array($this, 'get_cc_addresses'), 1, 10);

        add_filter('auction-date-formatting', array($this, 'format_auction_date'), 1, 10);
        add_filter('auctioneer-property-list', array($this, 'get_auction_property_list'), 1, 10);
        add_filter('auctioneer-related-list', array($this, 'related_items_list'), 1, 10);
        add_filter('auctioneer-related-csv', array($this, 'related_items_csv'), 1, 10);
        add_filter('auctioneer-nice-date', array($this, 'nice_date'), 1, 10);
        add_filter('auctioneer-manager-contact', array($this, 'manager_contact'), 2, 10);
        add_filter('auctioneer-address-format', array($this, 'format_address'), 3, 10);

        //round down some numbers
        add_filter('auctioneer-bldg-sqft', array($this, 'round_down'), 1, 10);
        add_filter('auctioneer-lot-sqft', array($this, 'round_down'), 1, 10);
        add_filter('auctioneer-acreage', array($this, 'drop_decimals'), 1, 10);

        //apply english number formatting
        add_filter('auctioneer-bldg-sqft', array($this, 'number_formatting'), 1, 11);
        add_filter('auctioneer-lot-sqft', array($this, 'number_formatting'), 1, 11);
        // add_filter('auctioneer-acreage', array($this, 'number_formatting'), 1, 11);

        // add_filter('auctioneer-sold-value', array($this, 'number_formatting'), 1, 10);
        add_filter('auctioneer-tax-value', array($this, 'number_formatting'), 1, 10);
        add_filter('auctioneer-year-taxes', array($this, 'number_formatting'), 1, 10);

        //leading dollar sign
        add_filter('auctioneer-sold-value', array($this, 'add_dollar_sign'), 1, 10);
        add_filter('auctioneer-tax-value', array($this, 'add_dollar_sign'), 1, 10);
        add_filter('auctioneer-year-taxes', array($this, 'add_dollar_sign'), 1, 10);

        //add plus/minus sign
        add_filter('auctioneer-bldg-sqft', array($this, 'add_plus_minus'), 1, 15);
        add_filter('auctioneer-lot-sqft', array($this, 'add_plus_minus'), 1, 15);
        add_filter('auctioneer-acreage', array($this, 'add_plus_minus'), 1, 15);
    }


    function meta_display($markup, $post_id){
        $custom = get_post_custom($post_id);

        $address = '<p class="vcard">';
        $address .= '<span class="street-address">';
        $address .= (isset($custom['_auction_address_2'][0])) ? $custom['_auction_address_2'][0] : '';
        $address .= (isset($custom['_auction_address_2'][0])) ? sprintf('<br>%s', $custom['_auction_address_2'][0]) : '';
        $address .= '</span>';

        $address .= (isset($custom['_auction_address_city'][0])) ? sprintf('<br><span class="locality">%s</span>', $custom['_auction_address_city'][0]) : '' ;
        $address .= (isset($custom['_auction_address_city'][0]) && isset($custom['_auction_address_state'][0])) ? ', ' : '';
        $address .= (isset($custom['_auction_address_state'][0])) ? sprintf('<span class="region">%s</span> ', $custom['_auction_address_state'][0]) : '' ;
        $address .= (isset($custom['_auction_address_zipcode'][0])) ? sprintf('<span class="postal-code">%s</span>', $custom['_auction_address_zipcode'][0]) : '' ;
        $address .= (isset($custom['_auction_address_country'][0])) ? sprintf('<b class="country">%s</span>', $custom['_auction_address_country'][0]) : '' ;
        $address .= '</p>';

        $markup .= $address;

        $date = '<p class="date">';
        $date .= sprintf('%s: %s', __('Start', 'auctioneer'), date('F j, Y', strtotime($custom['_auction_start_date'][0])));
        $date .= sprintf('<br />%s: %s', __('End', 'auctioneer'), date('F j, Y', strtotime($custom['_auction_end_date'][0])));
        $markup .= $date;

        return $markup;
    }

    function format_phone($number){
        if(strlen($number) == 10 ){
            if(  preg_match( '/^(\d{3})(\d{3})(\d{4})$/', $number,  $matches ) )
            {
                $number = $matches[1] . '-' .$matches[2] . '-' . $matches[3];
            }
        } elseif(strlen($number) == 7) {
            if(  preg_match( '/^(\d{3})(\d{4})$/', $number,  $matches ) )
            {
                $number = $matches[1] . '-' .$matches[2] . '-' . $matches[3];
            }
        }
        return $number;
    }

    function format_email($address){
        return antispambot($address);
    }

    function format_mailto($address){
        $mailto = sprintf('mailto:%s', $address);
        $mailto .= sprintf('?cc=%s', apply_filters('auctioneer-cc-addresses', ''));
        $mailto .= sprintf('&subject=%s', urlencode(__('Information Request: ', 'aucitoneer') . get_the_title() ));

        return $mailto;
    }

    function get_cc_addresses($addresses){
        $addresses = get_option('auctioneer_cc_addresses');
        if(!$addresses){
            return get_bloginfo('admin_email');
        } else {
            return implode(',', $addresses);
        }
    }

    function get_auction_property_list($list){
        return sprintf('<nav class="property-list">%s</nav>', $list);
    }

    function nice_date($date){
        return sprintf('<span class="date-time"><span class="date">%s</span>%s<span class="time">%s</span></span>', date('l, F j Y', $date), __(' at ', 'auctioneer'), date('g:i a', $date));
    }

    function format_auction_date($date){
        return (get_post_meta(get_the_id(), '_auction_date_tenative', true)) ? sprintf('%s(%s)', __('dates tenative', 'auctioneer')) : $date ;
    }

    /* expects the result of a wp query object */
    function related_items_list($items){
        if ( $items->have_posts() ) {
            $list = '<ul class="related-list">';
            while ( $items->have_posts() ) : $items->the_post();
                $list .= sprintf('<li class="related-list-item"><a class="related-item-link" href="%s" title="%s">%s</a></li>', get_permalink(), the_title_attribute('echo=0'), get_the_title());
            endwhile;
            $list .= '</ul>';
            return $list;
         } else {
            return '';
        }
    }

    /* expects the result of a wp query object */
    function related_items_csv($items){
        if ( $items->have_posts() ) {
            $total = $counter = $items->found_posts;
            $list = '<span class="related-csv">';
            while ( $items->have_posts() ) : $items->the_post();
                $counter--;
                switch ($counter){
                    default:
                        $sep = ', ';
                    break;
                    case 1:
                        $sep = ' ' . __('and', 'auctioneer') . ' ';
                    break;
                    case 0:
                        $sep = '';
                    break;
                }
                $list .= sprintf('<span class="related-csv-item"><a class="related-item-link" href="%s" title="%s">%s</a>%s</span>', get_permalink(), the_title_attribute('echo=0'), get_the_title(), $sep);
            endwhile;
            $list .= '</span>';
            return $list;
         } else {
            return '';
        }
    }

    function format_address($markup, $address){
        $markup .= '<address>';
        $markup .= $address['address_1'];
        $markup .= ($format = 'block') ? '<br />' : ' ' ;
        $markup .= ($address['address_2'] && '' != $address['address_2']) ? $address['address_2'] . '<br />' : '';

        $markup .= ($address['city'] && '' != $address['city']) ? $address['city'] . ', ': '';
        $markup .= ($address['state'] && '' != $address['state']) ? $address['state'] . ' ' : '';
        $markup .= ($address['zip'] && '' != $address['zip']) ? $address['zip'] : '';
        $markup .= '</address>';
        return $markup;
    }

    function manager_contact($text, $user_obj){
        $sales_manager_data = get_userdata($user_obj->ID);
        $sales_manager_meta =  get_user_meta($user_obj->ID);
        $contact_html = sprintf('<a href="#contact-sales" class="contact-sales-link" data-attr-manager-id="%d">%s</a>', $user_obj->ID, $sales_manager_data->display_name);

        $contact_html .= (isset($sales_manager_meta['phone_number'])) ? sprintf(' %s %s', __('at', 'auctioneer'), apply_filters('auctioneer-phone-format', $sales_manager_meta['phone_number'][0])) : '';
        $contact_html .=  sprintf('<a href="%s" class="contact-sales-email">%s</a>', apply_filters('auctioneer-mailto-format', $sales_manager_data->user_email), apply_filters('auctioneer-email-format', $sales_manager_data->user_email));
        return $contact_html;
    }

    function add_plus_minus($num){
        return '&plusmn; ' . $num;
    }

    function add_dollar_sign($num){
        return money_format('$%i', $num);
    }

    function number_formatting($num){
        return number_format($num);
    }

    function drop_decimals($num){
        if($num < 2){
            return floor(($num) * 100 ) * .01;
        } elseif ($num < 10 ) {
            return floor(($num) * 10 ) * .1;
        }else {
            return floor($num);
        }
    }

    function round_down($num){
         return round($num, -2);
    }
}
