<?php
namespace Auctioneer;

class Query{

    function __construct(Common $common){
        $this->common = $common;
        add_filter('pre_get_posts', array($this, 'modify_query'));
    }
    //modify's the built in wp_query
    function modify_query($query){
        if( $query->is_main_query()
            &&
            ($query->is_post_type_archive
                && $query->query['post_type'] == 'auctioneer_auction')
            || ($query->is_archive
                && isset($query->tax_query)
                && is_array($query->tax_query->queries)
                && isset($query->tax_query->queries[0])
                && $query->tax_query->queries[0]['taxonomy'] == 'auctioneer_type'))
        {
            $query->set('meta_key','_auction_start');

            $query->set('orderby', 'meta_value');
            $query->set('post_status', 'publish');
            $query->set('order', 'ASC');

        }


    }
}
