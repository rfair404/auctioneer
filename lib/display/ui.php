<?php
namespace Auctioneer;


class UI{
    function __construct(Common $common){
        $this->common = $common;
        add_action('init', array($this, 'register_css'), 10);
        add_action('wp_print_scripts', array($this, 'enqueue_css'), 10);
        add_filter('post_class', array($this, 'post_classes'), 10, 1);
        add_filter('the_title', array($this, 'alter_title'), 20, 1);
        add_filter('auctioneer-auction-status', array($this, 'auction_status'));
        add_filter('auctioneer-property-status', array($this, 'property_status'));

    }

    function register_css(){
        wp_register_style('auctioneer', $this->common->lib_url . 'display/css/auctioneer.min.css', array(), $this->common->version, 'screen');
    }

    function enqueue_css(){
        wp_enqueue_style('auctioneer');
    }
    function post_classes($classes){
        $classes[] = 'status-' . get_post_status();
        return $classes;
    }
    function alter_title($title){
        //don't use outside the loop , e.g. nav menus, widgets etc.
        if(!in_the_loop()){
            return $title;
        }
        //don't use except on the queried post (e.g. other posts within the content)
        if( is_singular()){
            $query_object = get_queried_object();
            if( $query_object->ID != get_the_id() ){
                return $title;
            }
        }
        //if it is an auction get the auction status
        if( get_post_type() == 'auctioneer_auction' ){
            $title = '<span class="title">' . $title . '</span>' . apply_filters('auctioneer-auction-status', get_the_id());
        }

        //or if it is a property get the property status
        if( get_post_type() == 'auctioneer_property'){
            $title = '<span class="title">' . $title . '</span>' . apply_filters('auctioneer-property-status', get_the_id());
        }
        return $title;
    }

    function auction_status($post_id){
        //if archived, set the status text to "archived"
        $status_text = ( get_post_status() == 'auctioneer_archive' ) ? __('Archived', 'auctioneer') : false;
        if(!$status_text){
            $tbd = (int) get_post_meta($post_id, '_auction_dates_tentative', true );
            $status_text = ($tbd) ? __('TBD', 'auctioneer') : false;
        }
        if(!$status_text){
            $start_date = (int) get_post_meta($post_id, '_auction_start', true );
            $start_delta = ( $start_date - time() >= 1 ) ? sprintf('%s %s', __('Starts in', 'auctioneer'), human_time_diff( time(), $start_date )) : false;
            $status_text = ($start_delta) ? $start_delta : false;
        }

        if(!$status_text){
            //still no status text perhaps it is now.
            $end_date = (int) get_post_meta($post_id, '_auction_end', true );
            $end_delta = ( $end_date - time() >= 1 ) ? sprintf('%s %s', __('Time remaining', 'auctioneer'), human_time_diff( time(), $end_date )) : false;
            $status_text = $end_delta;
        }


        $format = '<span class="status">%s</span>';
        return ($status_text) ? apply_filters('auctioneer-status-output', sprintf($format, $status_text)) : '';
    }

    function property_status($post_id){
        $status_text = (get_post_status() == 'auctioneer_sold' ) ? __('Sold', 'auctioneer') : false;
        $sold_value = (int) get_post_meta($post_id, '_property_sold_value', true );

        $status_text = ( $status_text && $sold_value ) ? sprintf( '%s: %s', $status_text, apply_filters('auctioneer-sold-value', $sold_value) ) : $status_text;


        $format = '<span class="status">%s</span>';
        return ($status_text) ? apply_filters('auctioneer-status-output', sprintf($format, $status_text)) : '';
    }
}
