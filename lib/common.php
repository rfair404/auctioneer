<?php

namespace Auctioneer;

/**
 * The Auctioneer Common Class controls the plugin stuff used "everywhere" meaning backend and frontend
 * This class is used both in the Auctioneer\Admin class and the Auctioneer\Display class
 */
class Common{

    public $version, $slug, $plugin_url, $plugin_dir, $lib_url, $lib_dir;

    function __construct(){
        $this->version = '1.3';
        $this->slug = str_replace('/lib/'.basename( __FILE__),"",plugin_basename(__FILE__));
        $this->lib_url = WP_PLUGIN_URL.'/'.str_replace(basename( __FILE__),"",plugin_basename(__FILE__));
        $this->lib_dir = WP_PLUGIN_DIR.'/'.str_replace(basename( __FILE__),"",plugin_basename(__FILE__));
        $this->plugin_url = str_replace("/lib/" , "/", $this->lib_url);
        $this->plugin_dir = str_replace("/lib/" , "/", $this->lib_dir);
    }

    /**
     * gets the version number
     * @static
     * @return int version
     */
    public static function get_version(){
        return $this->version;
    }

    /**
     * gets the plugin lib url
     * @static
     * @return string
     */
    public static function get_lib_url(){
        return $this->lib_url;
    }

    /**
     * gets the plugin lib directory
     * @static
     * @return string
     */
    public static function get_lib_dir(){
        return $this->lib_dir;
    }

    /**
     * gets the plugin slug
     * @static
     * @return mixed
     */
    public static function get_slug(){
        return $this->slug;
    }

}
