<?php

namespace Auctioneer;

/**
 * The Auctioneer InformationArchitecture Class controls the post types and taxonomies
 */
class InformationArchitecture
{
    public $common;
    function __construct(Common $common){
        $this->common = $common;
        add_action('init', array($this, 'register_post_types'), 10);
        add_action('init', array($this, 'register_taxonomies'), 10);
        add_action('init', array($this, 'post_status'), 10);
        add_filter('display_post_states', array($this, 'display_archive_state'));
        add_filter('display_post_states', array($this, 'display_sold_state'));
        add_filter('auctioneer-posttype-args', array($this, 'default_posttype_args'), 3, 10);
        add_filter('auctioneer-posttype-lables', array($this, 'default_posttype_labels'), 3, 10);
        add_filter('auctioneer-taxonomy-args', array($this, 'default_taxonomy_args'), 3, 10);
        add_action( 'p2p_init', array($this, 'p2p_connection_types') );
    }

    /**
     *
     */
    function register_post_types(){
        $type = 'auctioneer_auction';
        $type_args = apply_filters('auctioneer-posttype-args', 'auction', 'auctions', array() );
        register_post_type($type, $type_args);

        $type = 'auctioneer_property';
        $type_args = apply_filters('auctioneer-posttype-args', 'property', 'properties', array('menu_icon' => 'dashicons-admin-home') );
        register_post_type($type, $type_args);
    }

    /**
     *
     */
    function register_taxonomies(){

        $taxonomy = 'auctioneer_type';
        $taxonomy_args = apply_filters('auctioneer-taxonomy-args', __('Auction Type', 'auctioneer'), __('Auction Types', 'auctioneer'), array('rewrite' => array('slug' => 'auction-type')));
        register_taxonomy($taxonomy, 'auctioneer_auction', $taxonomy_args);

        $taxonomy = 'auctioneer_category';
        $taxonomy_args = apply_filters('auctioneer-taxonomy-args', __('Property Category', 'auctioneer'), __('Propercy Categories', 'auctioneer'), array('rewrite' => array('slug' => 'property-category')));
        register_taxonomy($taxonomy, 'auctioneer_property', $taxonomy_args);

        $taxonomy = 'auctioneer_zoning';
        $taxonomy_args = apply_filters('auctioneer-taxonomy-args', __('Property Zoning', 'auctioneer'), __('Property Zoning', 'auctioneer'), array( 'hierarchical' => false, 'rewrite' => array('slug' => 'property-zoning')));
        register_taxonomy($taxonomy, 'auctioneer_property', $taxonomy_args);
    }

    /**
     *
     */
    function post_status(){
        register_post_status( 'auctioneer_archive', array(
            'label'                     => _x( 'Archived', 'auctioneer_auction' ),
            'public'                    => true,
            'exclude_from_search'       => false,
            'show_in_admin_all_list'    => true,
            'show_in_admin_status_list' => true,
            'label_count'               => _n_noop( 'Archived <span class="count">(%s)</span>', 'Archived <span class="count">(%s)</span>' ),
        ) );

        register_post_status( 'auctioneer_sold', array(
            'label'                     => _x( 'Sold', 'auctioneer_sold' ),
            'public'                    => true,
            'exclude_from_search'       => false,
            'show_in_admin_all_list'    => true,
            'show_in_admin_status_list' => true,
            'label_count'               => _n_noop( 'Sold <span class="count">(%s)</span>', 'Sold <span class="count">(%s)</span>' ),
        ) );

    }

    /**
     * @param $states
     * @return array
     */
    function display_archive_state( $states ) {
        global $post;
        $arg = get_query_var( 'post_status' );
        if($arg != 'auctioneer_archive'){
            if($post->post_status == 'auctioneer_archive'){
                return array(__('Archived', 'auctioneer'));
            }
        }
        return $states;
    }

     /**
     * @param $states
     * @return array
     */
    function display_sold_state( $states ) {
        global $post;
        $arg = get_query_var( 'post_status' );
        if($arg != 'auctioneer_property'){
            if($post->post_status == 'auctioneer_sold'){
                return array(__('Sold', 'auctioneer'));
            }
        }
        return $states;
    }

    /**
     * @static
     * @param $singular
     * @param $plural
     * @param array $args
     * @return mixed
     */
    public static function default_posttype_args($singular, $plural, $args=array()){

        if ($plural===false)			$plural = $singular . 's';
        elseif ($plural===true)			$plural = $singular;

        $defaults = array(
            'labels'    => apply_filters('auctioneer-posttype-lables', $singular, $plural),
            'description' => __( sprintf('%s files', $singular) ), /* Custom Type Description */
            'public' => true,
            'publicly_queryable' => true,
            'exclude_from_search' => false,
            'show_ui' => true,
            'query_var' => true,
            'menu_position' => 4.10483,
            'menu_icon' => 'dashicons-tag',
            'rewrite' => array('slug' => $plural, 'with_front' => false ),
            'capability_type' => 'post',
            'hierarchical' => false,
            'has_archive' => true,
            'supports' => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt', 'revisions')
        );
        return wp_parse_args($args, $defaults);
    }

    /**
     * @static
     * @param $singular
     * @param bool $plural
     * @param array $args
     * @return mixed
     */
    public static function default_posttype_labels($singular,$plural=false,$args=array()) {

        $plural = ucfirst($plural);
        $singular = ucfirst($singular);

        $defaults = array(
            'name'			  	=>_x($plural,'post type general name'),
            'singular_name'		=>_x($singular,'post type singular name'),
            'add_new'		  	=>_x('Add New',$singular),
            'add_new_item'		=>__("Add New $singular"),
            'edit_item'		  	=>__("Edit $singular"),
            'new_item'		  	=>__("New $singular"),
            'view_item'		  	=>__("View $singular"),
            'search_items'		=>__("Search $plural"),
            'not_found'		  	=>__("No $plural Found"),
            'not_found_in_trash'=>__("No $plural Found in Trash"),
            'parent_item_colon' =>'',
        );
        return wp_parse_args($args,$defaults);
    }

    /**
     * @static
     * @param $singular
     * @param $plural
     * @param array $args
     * @return mixed
     */
    public static function default_taxonomy_args($singular, $plural, $args=array()){

        $singular = ucfirst($singular);
        $plural = ucfirst($plural);

         $defaults = array(
            'hierarchical' => true,
            'labels' => array(
                'name' => __( $singular ),
                'singular_name' => __( $singular ),
                'search_items' =>  __( "Search $plural" ),
                'all_items' => __( "All $plural" ),
                'parent_item' => __( "Parent $singular" ),
                'parent_item_colon' => __( "Parent $singular:" ),
                'edit_item' => __( "Edit $singular" ),
                'update_item' => __( "Update $singular" ),
                'add_new_item' => __( "Add New $singular" ),
                'new_item_name' => __( "New $singular Name" )
            ),
            'show_ui' => true,
            'query_var' => true,
            'rewrite' => array(
                'slug' => strtolower( $singular ),
                'with_front' => false
            )
        );

        return wp_parse_args($args, $defaults);
    }

    function p2p_connection_types() {
        if(!function_exists('p2p_register_connection_type')){
            return;
        }

        p2p_register_connection_type( array(
            'name' => 'auctioneer_auction_to_document',
            'from' => 'auctioneer_auction',
            'to' => 'document',
            'fields' => array(
                'title' => array(
                    'title' => __('Title', 'auctioneer'),
                    'type' => 'text',
                ),
            ),
        ) );

        p2p_register_connection_type( array(
            'name' => 'auctioneer_property_to_document',
            'from' => 'auctioneer_property',
            'to' => 'document',
            'fields' => array(
                'title' => array(
                    'title' => __('Title', 'auctioneer'),
                    'type' => 'text',
                ),
            ),
        ) );


         p2p_register_connection_type( array(
            'name' => 'auctioneer_auction_to_property',
            'from' => 'auctioneer_auction',
            'to' => 'auctioneer_property',
            'reciprocal' => false,
            'cardinality' => 'many-to-many',
            'sortable' => 'from',
            'title' => array(
              'from' => 'Included Properties',
              'to' => 'Auctions'
              )
        ) );

      p2p_register_connection_type( array(
          'name' => 'auction_manager',
          'from' => 'auctioneer_auction',
          'to' => 'user',
          'title' => array(
              'from' => 'Auction Manager',
              'to' => 'Auctions'
              )
      ) );

    }

}
