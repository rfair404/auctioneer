<?php

namespace Auctioneer;

class Automation{
    function __construct(Common $common){
        $this->common = $common;
        add_action('plugins_loaded', array($this, 'schedule_events'));
        add_filter('cron_schedules', array($this, 'set_ten_min_cron'));
        add_action('auctioneer-automation', array($this, 'archive_past_auctions'));
    }

    function schedule_events(){
        if ( ! wp_next_scheduled( 'auctioneer-automation' ) ) {
            $recurrence = apply_filters('auctioneer-cron-frequency', 'xmin');
            $args = array();
            wp_schedule_event(time(), $recurrence, 'auctioneer-automation', $args);
        }
    }


    // add once 10 minute interval to wp schedules
    function set_ten_min_cron($interval) {
        $interval['xmin'] = array('interval' => 10*60, 'display' => 'Every 10 minutes');
        return $interval;
    }

    function archive_past_auctions(){
        $expired_auctions = $this->get_expired_auctions();
        if($expired_auctions){
            foreach ( $expired_auctions as $expired_auction ){
                $auction = array(
                    'ID' => $expired_auction,
                    'post_status' => 'auctioneer_archive'
                );
                wp_update_post($auction);
            }
        }
    }

    function get_expired_auctions(){
        global $wpdb;
        $time = time();
        $query = $wpdb->prepare("
            SELECT ID FROM $wpdb->posts AS p
            LEFT JOIN $wpdb->postmeta AS pm
            ON p.ID = pm.post_id
            WHERE p.post_type = %s
            AND p.post_status != %s
            AND pm.meta_key = '_auction_end'
            AND pm.meta_value < %d
        ", 'auctioneer_auction', 'auctioneer_archive', $time);
        $auctions = $wpdb->get_col($query);
        return $auctions;
    }
}

