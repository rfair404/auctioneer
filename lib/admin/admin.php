<?php

namespace Auctioneer;
use Auctioneer\Common;

/**
 * The Auctioneer Admin Class controls things found only within the WordPress ACP (/wp-admin)
 * This class is only loaded when  'is_admin() == true'
 */

class Admin{
    public $common;
    function __construct(Common $common){
        $this->common = $common;
        //custom user role
        add_action('admin_init', array($this, 'create_role'), 10);
        //custom user profile fields
        add_action( 'show_user_profile', array($this, 'profile_fields') , 5);
        add_action( 'edit_user_profile', array($this, 'profile_fields') , 5);
        add_action( 'personal_options_update', array($this, 'profile_save' ), 10);
        add_action( 'edit_user_profile_update', array($this, 'profile_save' ), 10);
        //date and location on the edit auction pages
        add_filter('manage_edit-auctioneer_auction_columns', array($this, 'add_custom_column'), 1, 10);
        add_action('manage_auctioneer_auction_posts_custom_column' , array($this, 'custom_column_values'), 10, 2 );
        //some css&js love on the admin
        add_action('admin_init', array($this, 'register_assets'), 10);
        add_action('admin_print_styles-auctioneer', array($this, 'enqueue_css'));
        add_action('admin_print_styles', array($this, 'enqueue_css'), 10);
        add_action('admin_enqueue_scripts', array($this, 'enqueue_js'), 10);
    }

    /**
     * @static
     * Creates the Auctino Manager role if it doesn't exist
     *
     */
    public static function create_role(){
        //note... this is only run once, tupically on the first init after activation
        $result = get_role( 'auctioneer_auction_admin' );
        if(!$result){
            $editor = get_role('editor');
            $manager_roles = wp_parse_args(array(
                'read' => true, // True allows that capability
                'edit_posts' => true,
                'delete_posts' => false, // Nobody can delete posts,
                'manage_auctioneer_auctions' => true,
            ), $editor->capabilities);
            $result = add_role( 'auctioneer_auction_admin', 'Auction Administrator', $manager_roles);
            $manager = get_role('auctioneer_auction_admin');
            $manager->add_cap( 'edit_others_auctioneer_auctions' );
        }
    }

    /**
     * @static
     * Removes the auction manager role, not really used yet
     *
     */
    public static function remove_role(){
        remove_role('auctioneer_auction_admin');
    }

    /**
     * Adds the sales manager contact info form fields to the profile screen
     * @since 0.0.1
     * @author Russell Fair
     * @param obj $user the current user object
     */
    function profile_fields( $user ) { ?>
    <h3><?php _e('Sales Contact Information', 'auctioneer'); ?></h3>
    <table class="form-table">
        <tr>
            <th><label for="phone_number"><?php _e('Phone Number', 'auctioneer'); ?></label></th>
            <td>
                <input type="text" name="phone_number" id="phone_number" value="<?php echo esc_attr( get_the_author_meta( 'phone_number', $user->ID ) ); ?>" class="regular-text" /><br />
                <span class="description"><?php _e('Please enter your phone number, this will be publicly displayed.', 'auctioneer'); ?></span>
            </td>
        </tr>
        <tr>
            <th><label for="license_number"><?php _e('License Number', 'auctioneer'); ?></label></th>
            <td>
                <input type="text" name="license_number" id="license_number" value="<?php echo esc_attr( get_the_author_meta( 'license_number', $user->ID ) ); ?>" class="regular-text" /><br />
                <span class="description"><?php _e('Please enter your auctioneer license number, this will be publicly displayed.', 'auctioneer'); ?></span>
            </td>
        </tr>
    </table>
    <?php }

    /**
     * Saves the posted author bio information to the db if it is submitted
     * @since 0.0.2
     * @author Russell Fair
     * @param int $user_id the user to save data
     */
    function profile_save( $user_id ) {

        if ( !current_user_can( 'edit_user', $user_id ) )
            return false;
        update_user_meta( $user_id, 'phone_number', $_POST['phone_number'] );
        update_user_meta( $user_id, 'license_number', $_POST['license_number'] );

    }

    /**
     * Adds the Auction Date and Location Columns on the edit screen
     * @param $columns
     * @return array
     */
    function add_custom_column($columns){

        $date = $columns['date'];
        unset($columns['date']);
        $columns['auction_start'] = __( 'Auction Start', 'auctioneer' );
        $columns['auction_end'] = __( 'Auction End', 'auctioneer' );
        $columns['auction_city'] = __('City', 'auctioneer');
        $columns['auction_state'] = __('State', 'auctioneer');
        $columns['date'] = __('Published', 'auctioneer');

        return $columns;
    }

    /**
     * Gets the Auction Date and Location Values on the edit screen
     * @param $column
     * @param $post_id
     */
    function custom_column_values( $column, $post_id ) {
        switch ( $column ) {
            case 'auction_start' :
                echo date('F j, Y', get_post_meta($post_id, '_auction_start', true));
            break;
            case 'auction_end':
                echo date('F j, Y', get_post_meta($post_id, '_auction_end', true));
            break;
            case 'auction_city' :
               echo get_post_meta($post_id, '_auction_city', true);
            break;
            case 'auction_state':
                echo get_post_meta($post_id, '_auction_state', true);
            break;

        }
    }

    /**
     * Registers our custom css & javascripts
     */
    function register_assets(){
        wp_register_style('auctioneer-edit', $this->common->lib_url . 'admin/css/auctioneer-edit.min.css', array(), $this->common->version, 'all' );
        wp_register_style('auctioneer-settings', $this->common->lib_url . 'admin/css/auctioneer-settings.min.css', array(), $this->common->version, 'all' );
        wp_register_script('auctioneer-misc', $this->common->lib_url . 'admin/js/auctioneer-misc.js', array('jquery'), $this->common->version, true);
    }

    function enqueue_css(){
        global $pagenow;
        if(in_array($pagenow, array('post-new.php', 'post.php')) && in_array(get_post_type(), array( 'auctioneer_auction', 'auctioneer_property'))){
            wp_enqueue_style('auctioneer-edit');
        }
        if($pagenow == 'options-general.php' && isset($_GET['page']) && $_GET['page'] == 'auctioneer'){
            wp_enqueue_style('auctioneer-settings');
        }
    }

    /**
     * Registers our custom css & js
     */
    function enqueue_js(){
        global $post, $pagenow;
        if($pagenow == 'edit.php' && get_post_type() == 'auctioneer_auction'){
            wp_enqueue_script( 'jquery-ui-datepicker' );
        }
        if(in_array($pagenow, array('edit.php', 'post.php')) && in_array(get_post_type(), array('auctioneer_auction', 'auctioneer_property'))){
            switch (get_post_type()){
                case 'auctioneer_auction':
                    $auction_strings = array(
                        'statusText' => __('Archived', 'auctioneer'),
                        'statusValue' => 'auctioneer_archive',
                        'statusSelected' => ($post->post_status == 'auctioneer_archive') ? ' selected=\"selected\"' : '',
                        'statusLabel' => ($post->post_status == 'auctioneer_archive') ? __('Archived', 'auctioneer') : '',
                    );
                break;
                case 'auctioneer_property':
                    $auction_strings = array(
                        'statusText' => __('Sold', 'auctioneer'),
                        'statusValue' => 'auctioneer_sold',
                        'statusSelected' => ($post->post_status == 'auctioneer_sold') ? ' selected=\"selected\"' : '',
                        'statusLabel' => ($post->post_status == 'auctioneer_sold') ? __('Sold', 'auctioneer') : '',
                    );
                break;
            }

            wp_enqueue_script( 'auctioneer-misc' );
            wp_localize_script('auctioneer-misc', 'auctioneer', $auction_strings);
        }
    }
}
