<?php
namespace Auctioneer;
use Auctioneer\Common;
/**
 * Sets up some default taxonomy objects.
 */
class Settings
{
    function __construct(Common $common){
        add_action('admin_menu', array($this, 'register_menu'));
        add_action('admin_init', array($this, 'register_settings'), 10);
        add_action('admin_init', array($this, 'register_sections'), 15);
        add_action('admin_init', array($this, 'register_fields'), 20);
        add_action('save_post', array($this, '_select_default_content'), 2, 10);
    }

    function register_menu(){
        $menu_page = add_options_page(
            __('Auction Settings', 'auctioneer'),
            __('Auctions', 'auctioneer'),
            'publish_posts',
            'auctioneer',
            array($this, '_auctioneer_main_callback')
            ); //this actually could be used again?
    }

    function _auctioneer_main_callback(){ ?>
        <div class="wrap auctioneer-settings-screen">
        <div id="icon-auctioneer" class="icon32 dashicon dashicon-admin-tools">
        </div>
        <h2><?php _e('Auction Settings', 'auctioneer'); ?></h2>

        <?php $active_tab = isset( $_GET[ 'tab' ] ) ? $_GET[ 'tab' ] : 'settings';  ?>
            <h2 class="nav-tab-wrapper">
                <a href="?page=auctioneer&tab=settings" class="tab-main nav-tab <?php echo $active_tab == 'settings' ? 'nav-tab-active' : ''; ?>"><?php _e('Settings', 'auctioneer'); ?></a>
                <a href="?page=auctioneer&tab=help" class="tab-main nav-tab <?php echo $active_tab == 'help' ? 'nav-tab-active' : ''; ?>"><?php _e('Help', 'auctioneer'); ?></a>
            </h2>
            <?php if('settings' == $active_tab) { ?>
            <form method="POST" action="options.php">
                <?php settings_fields( 'auctioneer_settings' ); ?>
                <?php do_settings_sections( 'auctioneer' ); ?>
                <?php submit_button();?>
            </form>
            <?php } else { ?>
                <p><?php _e('This plugin uses shortcodes to display improtant content inside of Auctions and Property listings.', 'auctioneer');?></p>
                <p><?php _e('To use a shortcode insert the shortcode (plus any available options) inside of brackets, like <b>[shortcode_name option="value"]</b>.', 'auctioneer');?></p>
                <?php $shortcodes = $this->shortcode_examples();
                if($shortcodes && is_array($shortcodes)) { ?>
                    <table class="shortcode-reference">
                        <thead><tr><th><?php _e('shortcode', 'auctioneer'); ?></th><th><?php _e('description', 'auctioneer');?></th><th><?php _e('options', 'auctioneer'); ?></th></tr></thead>
                        <tbody>
                        <?php foreach ($shortcodes as $shortcode => $context){
                            printf('<tr><td class="shortcode-example"><b>[%s]</b></td><td class="shortcode-description">%s</td><td>%s</td></tr>', $shortcode, $context['description'], $context['options']);
                        } ?>
                    </tbody>
                </table>
                <?php } ?>
            <?php } ?>
        </div>
<?php
    }

    function shortcode_examples(){
        $shortcodes = array(
            'auction_properties' => array(
                'description' => __('Displays the connected properties for a given auction.', 'auctioneer'),
                'options' => __('format="list" ( bullet list )<br /> format="csv" ( comma seperated list )', 'auctioneer')
            ),
            'auction_date' => array(
                'description' => __('Displays the auction date and time.', 'auctioneer'),
                'options' => __('start ( start date only )<br />end ( end date only )', 'auctioneer')
            ),
            'auction_address' => array(
                'description' => __('Displays the auction address or bidding url', 'auctioneer'),
                'options' => __('none', 'auctioneer')
            ),
            'auction_manager' => array(
                'description' => __('Displays the auction manager contact information', 'auctioneer'),
                'options' => __('none', 'auctioneer')
            ),
            'property_address' => array(
                'description' => __('Displays the property address', 'auctioneer'),
                'options' => __('none', 'auctioneer')
            ),
            //these need work still
            'property_map' => array(
                'description' => __('Displays the property map', 'auctioneer'),
                'options' => __('format="link" ( displays a link to the map )<br />format="embed" ( embed the map )', 'auctioneer')
            ),
            'property_details' => array(
                'description' => __('Displays the property details', 'auctioneer'),
                'options' => __('format="table" ( displays details as table )<br />format="list" ( displays details as list )', 'auctioneer')
            ),
            'property_zoning' => array(
                'description' => __('Displays the property zoning', 'auctioneer'),
                'options' => __('none', 'auctioneer')
            )

        );
        return $shortcodes;
    }

    function register_settings(){
        register_setting('auctioneer_settings', 'auctioneer_settings', array($this, '_sanitize_content'));
       }

    function register_sections(){
        add_settings_section(
            'auction_settings',
            __('Auction Settings', 'auctioneer'),
            array($this, '_auction_settings'),
            'auctioneer'
        );

        add_settings_section(
            'property_settings',
            __('Property Settings', 'auctioneer'),
            array($this, '_property_settings'),
            'auctioneer'
        );
    }

    function register_fields(){
        add_settings_field(
            'auctioneer_content_template',
            __('Default Auction Template', 'auctioneer'),
            array($this, '_auction_template'),
            'auctioneer',
            'auction_settings'
        );

        add_settings_field(
            'auctioneer_content_template',
            __('Default Property Template', 'auctioneer'),
            array($this, '_property_template'),
            'auctioneer',
            'property_settings'
        );
    }
    //section
    function _auction_settings(){
        _e('Use the fields in this section to configure the defaults for creating new auctions');
    }

    //section
    function _property_settings(){
        _e('Use the fields in this section to configure the defaults for creating new properties');
    }

    //auction template field
    function _auction_template(){
        $settings = get_option('auctioneer_settings');
        $template = (isset($settings['auction_template'])) ? $settings['auction_template'] : ''; ?>
        <textarea name="auctioneer_settings[auction_template]" id="auction_template" cols="20"><?php echo esc_textarea($template); ?></textarea>
        <p><span class="note">
        <?php _e('Note: See list of available shortcodes in the help tab', 'auctioneer'); ?>
        </span></p>
        <?php
    }

    //property template field
    function _property_template(){
        $settings = get_option('auctioneer_settings');
        $template = (isset($settings['property_template'])) ? $settings['property_template'] : ''; ?>
        <textarea name="auctioneer_settings[property_template]" id="property_template" cols="20"><?php echo esc_textarea($template); ?></textarea>
        <p><span class="note">
        <?php _e('Note: See list of available shortcodes in the help tab', 'auctioneer'); ?>
        </span></p>
        <?php
    }

    function _sanitize_content($options){
        return $options;
    }

    function _select_default_content($post_id, $post_obj){
        if($post_obj->post_status != 'auto-draft'){
            return;
        }

        switch($post_obj->post_type){
            case 'auctioneer_auction':
                add_filter( 'default_content', array($this, '_default_auction_content'));
            break;
            case 'auctioneer_property':
                add_filter( 'default_content', array($this, '_default_property_content'));
            break;
        }
    }

    function _default_auction_content( $content ) {
        $settings = get_option('auctioneer_settings');
        $content = (isset($settings['auction_template'])) ? $settings['auction_template'] : '';
        return $content;
    }

    function _default_property_content( $content ) {
        $settings = get_option('auctioneer_settings');
        $content = (isset($settings['property_template'])) ? $settings['property_template'] : '';
        return $content;
    }
}
