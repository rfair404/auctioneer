<?php

namespace Auctioneer;
use Auctioneer\Common;

/**
 * Registers the custom meta boxes for use on the admin
 */

class Metaboxes{
    function __construct(Common $common){
        add_action('add_meta_boxes', array($this, 'register_metaboxes'), 10);
        add_action('save_post', array($this, 'save_auction_date'), 10);
        add_action('save_post', array($this, 'save_auction_info'), 10);
        add_action('save_post', array($this, 'save_property_location'), 10);
        add_action('save_post', array($this, 'save_property_info'), 10);
    }

    function register_metaboxes(){
        //auction date
        add_meta_box('auction_date', __('Auction Date', 'auctioneer'), array($this, 'date_callback'), 'auctioneer_auction', 'side', 'high', array());
        //external auction info
        add_meta_box('auction_info', __('Auction Information', 'auctioneer'), array($this, 'auction_info_callback'), 'auctioneer_auction', 'normal', 'high', array());
        //  property address and location info
        add_meta_box('property_location', __('Property Location', 'auctioneer'), array($this, 'property_location_callback'), 'auctioneer_property', 'normal', 'high', array());
        //  property info
        add_meta_box('property_info', __('Property Information', 'auctioneer'), array($this, 'property_info_callback'), 'auctioneer_property', 'normal', 'high', array());
      }

    function date_callback($post){
        wp_nonce_field( 'auction_date', 'auction_date_nonce' );
        // start date
        $startdate = $starttime = ( get_post_meta( $post->ID, '_auction_start', true ) ) ? get_post_meta( $post->ID, '_auction_start', true ) : time();
        echo '<label for="auction_start_date">';
        _e( "Start Date and Time:", 'auctioneer' );
        echo '</label> ';
        echo '<input type="date" id="auction_start_date" name="auction_start_date" value="' . esc_attr( date('Y-m-d', $startdate) ) . '" size="15" />';
        //start time
        echo '<input type="time" id="auction_start_time" name="auction_start_time" value="' . esc_attr( date('H:i', $starttime) ) . '" size="10" />';

        //end date
        $enddate = $endtime = ( get_post_meta( $post->ID, '_auction_end', true ) ) ?  get_post_meta( $post->ID, '_auction_end', true ) : time();
        echo '<p><label for="auction_end_date">';
        _e( "End Date and Time:", 'auctioneer' );
        echo '</label> ';
        echo '<input type="date" id="auction_end_date" name="auction_end_date" value="' . esc_attr( date('Y-m-d', $enddate) ) . '" size="15" />';
        //end time
        echo '<input type="time" id="auction_end_time" name="auction_end_time" value="' . esc_attr( date('H:i', $endtime) ) . '" size="10" />';

        //timezone
        echo '<p><label for="auction_time_zone">';
        _e( "Timezone:", 'auctioneer' );
        $timezone = ( get_post_meta( $post->ID, '_auciton_time_zone', true) ) ? get_post_meta( $post->ID, '_auction_time_zone', true ) : date('T', time()) ;
        echo '<input type="text" id="auction_time_zone" name="auction_time_zone" value="' . esc_attr( $timezone ). '" size="10" />';

        //tentative
        $tbd = get_post_meta($post->ID, '_auction_dates_tentative', true);
        $checked = ($tbd) ? 'checked' : '';
        echo '<p><label for="auction_dates_tentative">';
        _e('Auction date is tentative.', 'auctioneer');
        printf('<input type="checkbox" id="auction_dates_tentative" name="auction_dates_tentative"%s>', $checked);

    }

    function auction_info_callback($post){
        wp_nonce_field( 'auction_info', 'auction_info_nonce' );
        //bidding url
        $url = get_post_meta( $post->ID, '_auction_external_url', true );
        echo '<label for="auction_external_url">';
        _e( "External bidding url:", 'auctioneer' );
        echo '</label> ';
        echo '<input type="url" id="auction_external_url" name="auction_external_url" value="' . esc_attr( esc_url( $url ) ) . '" size="25" />';

         $add1 = get_post_meta( $post->ID, '_auction_address_1', true );
        echo '<p><label for="auction_address_1">';
        _e( "Address 1:", 'auctioneer' );
        echo '</label> ';
        echo '<input type="text" id="auction_address_1" name="auction_address_1" value="' . esc_attr( $add1 ) . '" size="25" />';

         $add2 = get_post_meta( $post->ID, '_auction_address_2', true );
        echo '<label for="auction_address_2">';
        _e( "Address 2:", 'auctioneer' );
        echo '</label> ';
        echo '<input type="text" id="auction_address_2" name="auction_address_2" value="' . esc_attr( $add2 ) . '" size="25" />';

         $add3 = get_post_meta( $post->ID, '_auction_address_3', true );
        echo '<label for="auction_address_3">';
        _e( "Address 3:", 'auctioneer' );
        echo '</label> ';
        echo '<input type="text" id="auction_address_3" name="auction_address_3" value="' . esc_attr( $add3 ) . '" size="25" />';

         $city = get_post_meta( $post->ID, '_auction_address_city', true );
        echo '<p><label for="auction_address_city">';
        _e( "City:", 'auctioneer' );
        echo '</label> ';
        echo '<input type="text" id="auction_address_city" name="auction_address_city" value="' . esc_attr( $city ) . '" size="25" />';

        $state = get_post_meta( $post->ID, '_auction_address_state', true );
        echo '<label for="auction_address_state">';
        _e( "State:", 'auctioneer' );
        echo '</label> ';
        $this->state_picker('auction_address_state', $state);

         $zip = get_post_meta( $post->ID, '_auction_address_zip', true );
        echo '<label for="auction_address_zip">';
        _e( "Zip:", 'auctioneer' );
        echo '</label> ';
        echo '<input type="number" id="auction_address_zip" name="auction_address_zip" value="' . esc_attr( $zip ) . '" size="10" />';

    }

    function property_location_callback($post){
        wp_nonce_field( 'property_location', 'property_location_nonce' );
        $map_url = get_post_meta( $post->ID, '_map_url', true );
        echo '<label for="map_url">';
        _e( "Map URL:", 'auctioneer' );
        echo '</label> ';
        echo '<br /><input type="url" id="map_url" name="map_url" value="' . esc_attr( esc_url( $map_url ) ) . '" size="25" />';
        $directions = get_post_meta( $post->ID, '_property_directions', true );
        echo '<br />';
        echo '<label for="property_directions">';
        _e( "Directions:", 'auctioneer' );
        echo '</label> ';
        echo '<br /><textarea id="property_directions" name="property_directions">';
        echo esc_textarea( $directions ) ;
        echo '</textarea>';

        //street address
        $address_1 = get_post_meta( $post->ID, '_property_address_1', true );
        echo '<p><label for="property_address_1">';
        _e( "Address:", 'auctioneer' );
        echo '</label> ';
        echo '<input type="text" id="property_address_1" name="property_address_1" value="' . esc_attr( $address_1 ) . '" size="25" />';
        //secondary address
        $address_2 = get_post_meta( $post->ID, '_property_address_2', true );
        echo '<label for="property_address_2">';
        _e( "Address 2:", 'auctioneer' );
        echo '</label> ';
        echo '<input type="text" id="property_address_2" name="property_address_2" value="' . esc_attr( $address_2 ) . '" size="25" />';

         $address_3 = get_post_meta( $post->ID, '_property_address_3', true );
        echo '<label for="property_address_3">';
        _e( "Address 3:", 'auctioneer' );
        echo '</label> ';
        echo '<input type="text" id="property_address_3" name="property_address_3" value="' . esc_attr( $address_3 ) . '" size="25" />';

  //city
        echo '<p><label for="property_address_city">';
        _e( "City:", 'auctioneer' );
        echo '</label> ';
        $address_city = get_post_meta( $post->ID, '_property_address_city', true );
        echo '<input type="text" id="property_address_city" name="property_address_city" value="' . esc_attr( $address_city ) . '" size="25" />';
        //state
        $address_state = get_post_meta( $post->ID, '_property_address_state', true );
        echo '<label for="property_address_state">';
        _e( "State:", 'auctioneer' );
        echo '</label> ';
        $this->state_picker('property_address_state', $address_state);

        // echo '<input type="text" id="property_address_state" name="property_address_state" value="' . esc_attr( $address_state ) . '" size="5" />';
        //zipcode
        $address_zip = get_post_meta( $post->ID, '_property_address_zip', true );
        echo '<label for="property_address_zip">';
        _e( "Zip:", 'auctioneer' );
        echo '</label> ';
        echo '<input type="text" id="property_address_zipcode" name="property_address_zip" value="' . esc_attr( $address_zip ) . '" size="10" />';
        //future proof
        $geolocation_latitude = get_post_meta( $post->ID, '_property_geolocation_latitude', true );
        echo '<p><label for="property_geolocation_latitude">';
        _e( "Latitude:", 'auctioneer' );
        echo '</label> ';
        echo '<input type="text" disabled id="property_geolocation_latitude" name="property_geolocation_latitude" value="' . esc_attr( $geolocation_latitude ) . '" size="25" />';
        //longitude
        $geolocation_longitude = get_post_meta( $post->ID, '_property_geolocation_longitude', true );
        echo '<label for="property_geolocation_longitude">';
        _e( "Longitude:", 'auctioneer' );
        echo '</label> ';
        echo '<input type="text" disabled id="property_geolocation_longitude" name="property_geolocation_longitude" value="' . esc_attr( $geolocation_longitude ) . '" size="25" />';

    }

    function property_info_callback($post){
        wp_nonce_field( 'property_info', 'property_info_nonce' );
        //bidding url
        $bed_rooms = get_post_meta( $post->ID, '_property_bedrooms', true );
        echo '<p><label for="property_bedrooms">';
        _e( "Bedrooms:", 'auctioneer' );
        echo '</label> ';
        echo '<input type="number" id="property_bedrooms" name="property_bedrooms" step="0.5" value="' . esc_attr( $bed_rooms ) . '" size="5" />';

         $baths = get_post_meta( $post->ID, '_property_bathrooms', true );
        echo '<label for="property_bathrooms">';
        _e( "Bathrooms:", 'auctioneer' );
        echo '</label> ';
        echo '<input type="number" id="property_bathrooms" name="property_bathrooms" step="0.5" value="' . esc_attr( $baths ) . '" size="5" />';

         $bldg_sqfootage = get_post_meta( $post->ID, '_property_bldg_squarefootage', true );
        echo '<label for="property_bldg_squarefootage">';
        _e( "Building Sq. Footage:", 'auctioneer' );
        echo '</label> ';
        echo '<input type="number" id="property_bldg_squarefootage" name="property_bldg_squarefootage" value="' . esc_attr( $bldg_sqfootage ) . '" size="5" />';

         $lot_sqfootage = get_post_meta( $post->ID, '_property_lot_sqfootage', true );
        echo '<p><label for="property_lot_sqfootage">';
        _e( "Lot Sq. Footage:", 'auctioneer' );
        echo '</label> ';
        echo '<input type="number" id="property_lot_sqfootage" name="property_lot_sqfootage" step="any" value="' . esc_attr( $lot_sqfootage ) . '" size="5" />';

         $acreage = get_post_meta( $post->ID, '_property_acreage', true );
        echo '<label for="property_acreage">';
        _e( "Acreage:", 'auctioneer' );
        echo '</label> ';
        echo '<input type="number" id="property_acreage" name="property_acreage" step="any" value="' . esc_attr( $acreage ) . '" size="5" />';

         $year = get_post_meta( $post->ID, '_property_year_built', true );
        echo '<label for="property_year_built">';
        _e( "Year Built:", 'auctioneer' );
        echo '</label> ';
        echo '<input type="text" id="property_year_built" name="property_year_built" value="' . esc_attr( $year ) . '" size="5" />';

        $parcel_num = get_post_meta( $post->ID, '_property_parcel_number', true );
        echo '<p><label for="property_parcel_number">';
        _e( "Parcel Number:", 'auctioneer' );
        echo '</label> ';
        echo '<input type="text" id="property_parcel_number" name="property_parcel_number" value="' . esc_attr( $parcel_num ) . '" size="15" />';

        $tax_value = get_post_meta( $post->ID, '_property_tax_value', true );
        echo '<label for="property_tax_value">';
        _e( "Property Tax Value:", 'auctioneer' );
        echo '</label> ';
        echo '<input type="number" id="property_tax_value" step="any" name="property_tax_value" value="' . esc_attr( $tax_value ) . '" size="5" />';

        $year_tax = get_post_meta( $post->ID, '_property_year_taxes', true );
        echo '<label for="property_year_taxes">';
        _e( "Current Year Taxes:", 'auctioneer' );
        echo '</label> ';
        echo '<input type="number" id="property_year_taxes" step="any" name="property_year_taxes" value="' . esc_attr( $year_tax ) . '" size="5" />';

        echo '<hr />';
        $sold_value = get_post_meta( $post->ID, '_property_sold_value', true );
        echo '<label for="property_sold_value">';
        _e( "Sold Value:", 'auctioneer' );
        echo '</label> ';
        echo '<input type="number" id="property_sold_value" step="any" name="property_sold_value" value="' . esc_attr( $sold_value) . '" size="5" />';
        echo '<p class="note">This only displays when a property is listed as "Sold"</p>';


    }

    function save_auction_date($post_id){
        if ( ! isset( $_POST['auction_date_nonce'] ) )
            return $post_id;
        $nonce = $_POST['auction_date_nonce'];
        if ( ! wp_verify_nonce( $nonce, 'auction_date' ) )
            return $post_id;
        if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE )
            return $post_id;
        if ( ! current_user_can( 'edit_post', $post_id ) ){
            return $post_id;
        }
        $startdate = sanitize_text_field( $_POST['auction_start_date'] );
        $enddate = sanitize_text_field( $_POST['auction_end_date'] );
        $starttime = sanitize_text_field( $_POST['auction_start_time'] );
        $endtime = sanitize_text_field( $_POST['auction_end_time'] );
        $time_zone = sanitize_text_field( $_POST['auction_time_zone'] );

        $tentative = (isset($_POST['auction_dates_tentative'])) ? true : false ;
        update_post_meta($post_id, '_auction_start', strtotime($startdate . ' ' . $starttime));
        update_post_meta($post_id, '_auction_end', strtotime($enddate . ' ' . $endtime));

        update_post_meta($post_id, '_auction_timezone', strtotime($enddate . ' ' . $endtime));
        update_post_meta($post_id, '_auction_dates_tentative', $tentative);
    }

    function save_auction_info($post_id){

        if ( ! isset( $_POST['auction_info_nonce'] ) )
            return $post_id;
        $nonce = $_POST['auction_info_nonce'];
        if ( ! wp_verify_nonce( $nonce, 'auction_info' ) )
            return $post_id;
        if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE )
            return $post_id;
        if ( ! current_user_can( 'edit_post', $post_id ) ){
            return $post_id;
        }

        // Confirmed
        $url = sanitize_text_field( $_POST['auction_external_url'] );
                $url = ($url) ? update_post_meta( $post_id, '_auction_external_url', $url ) : delete_post_meta($post_id, '_auction_external_url');
        $add1 = sanitize_text_field( $_POST['auction_address_1'] );
                $add1 = ($add1) ? update_post_meta( $post_id, '_auction_address_1', $add1 ) : delete_post_meta($post_id, '_auction_address_1');
        $add2 = sanitize_text_field( $_POST['auction_address_2'] );
                $add2 = ($add2) ? update_post_meta( $post_id, '_auction_address_2', $add2 ) : delete_post_meta($post_id, '_auction_address_2');
        $city = sanitize_text_field( $_POST['auction_address_city'] );
                $city = ($city) ? update_post_meta( $post_id, '_auction_address_city', $city ) : delete_post_meta($post_id, '_auction_address_city');
        $state = sanitize_text_field( $_POST['auction_address_state'] );
                $state = ($state) ? update_post_meta( $post_id, '_auction_address_state', $state ) : delete_post_meta($post_id, '_auction_address_state');
        $zip = sanitize_text_field( $_POST['auction_address_zip'] );
                $zip = ($zip) ? update_post_meta( $post_id, '_auction_address_zip', $zip ) : delete_post_meta($post_id, '_auction_address_zip');

        //get remainder of property address info and save it.
    }

    function save_property_location($post_id){
        if ( ! isset( $_POST['property_location_nonce'] ) )
            return $post_id;
        $nonce = $_POST['property_location_nonce'];
        if ( ! wp_verify_nonce( $nonce, 'property_location' ) )
            return $post_id;
        if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE )
            return $post_id;
        if ( ! current_user_can( 'edit_post', $post_id ) ){
            return $post_id;
        }

        $address_1 = sanitize_text_field( $_POST['property_address_1'] );
            $address_1 = ($address_1) ? update_post_meta( $post_id, '_property_address_1', $address_1 ) : delete_post_meta($post_id, '_property_address_1' );
        $address_2 = sanitize_text_field( $_POST['property_address_2'] );
            $address_2 = ($address_2) ? update_post_meta( $post_id, '_property_address_2', $address_2 ) : delete_post_meta($post_id, '_property_address_2' );
         $address_3 = sanitize_text_field( $_POST['property_address_3'] );
            $address_3 = ($address_3) ? update_post_meta( $post_id, '_property_address_3', $address_3 ) : delete_post_meta($post_id, '_property_address_3' );
        $address_city = sanitize_text_field( $_POST['property_address_city'] );
            $address_city = ($address_city) ? update_post_meta( $post_id, '_property_address_city', $address_city ): delete_post_meta($post_id, '_property_address_city');
        $address_state = sanitize_text_field( $_POST['property_address_state'] );
            $address_state = ($address_state) ? update_post_meta( $post_id, '_property_address_state', $address_state ) : delete_post_meta($post_id, '_property_address_state');
        $address_zipcode = sanitize_text_field( $_POST['property_address_zip'] );
            $address_zipcode =  ($address_zipcode) ? update_post_meta( $post_id, '_property_address_state', $address_state ): delete_post_meta($post_id, '_property_address_state');
        $property_map_url = sanitize_text_field( $_POST['map_url'] );
            $property_map_url = ($property_map_url) ? update_post_meta( $post_id, '_map_url', $property_map_url ) : delete_post_meta($post_id, '_map_url');
    }

    function save_property_info($post_id){
        if ( ! isset( $_POST['property_info_nonce'] ) )
            return $post_id;
        $nonce = $_POST['property_info_nonce'];
        if ( ! wp_verify_nonce( $nonce, 'property_info' ) )
            return $post_id;
        if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE )
            return $post_id;
        if ( ! current_user_can( 'edit_post', $post_id ) ){
            return $post_id;
        }

        $bedrooms = sanitize_text_field( $_POST['property_bedrooms']);
            $bedrooms = ($bedrooms) ? update_post_meta($post_id, '_property_bedrooms',$bedrooms) : delete_post_meta($post_id, '_property_bedrooms');
        $bathrooms= sanitize_text_field( $_POST['property_bathrooms']);
            $bathrooms = ($bathrooms) ? update_post_meta($post_id, '_property_bathrooms', $bathrooms) : delete_post_meta($post_id, '_property_bathrooms');
        $bldg_squarefootage   = sanitize_text_field( $_POST['property_bldg_squarefootage']);
            $bldg_squarefootage = ($bldg_squarefootage) ? update_post_meta($post_id, '_property_bldg_squarefootage', $bldg_squarefootage) : delete_post_meta($post_id, '_property_bldg_squarefootage');
        $acreage  = sanitize_text_field( $_POST['property_acreage']);
            $acreage = ($acreage) ? update_post_meta($post_id, '_property_acreage', $acreage) : delete_post_meta($post_id, '_property_acreage');
        $lot_sqfootage= sanitize_text_field( $_POST['property_lot_sqfootage']);
            $lot_sqfootage = ($lot_sqfootage) ? update_post_meta($post_id, '_property_lot_sqfootage', $lot_sqfootage) : delete_post_meta($post_id, '_property_lot_sqfootage');
        $year_built   = sanitize_text_field( $_POST['property_year_built']);
            $year_built = ($year_built) ? update_post_meta($post_id, '_property_year_built', $year_built) : delete_post_meta($post_id, '_property_year_built');
        $parcel_number= sanitize_text_field( $_POST['property_parcel_number']);
            $parcel_number = ($parcel_number) ? update_post_meta($post_id, '_property_parcel_number', $parcel_number) : delete_post_meta($post_id, '_property_parcel_number');
        $tax_value= sanitize_text_field( $_POST['property_tax_value']);
            $tax_value = ($tax_value) ? update_post_meta($post_id, '_property_tax_value', $tax_value) : delete_post_meta($post_id, '_property_tax_value');
        $year_taxes   = sanitize_text_field( $_POST['property_year_taxes']);
            $year_taxes = ($year_taxes) ? update_post_meta($post_id, '_property_year_taxes', $year_taxes) : delete_post_meta($post_id, '_property_year_taxes');

        $sold_value   = sanitize_text_field( $_POST['property_sold_value']);
            $sold_value = ($sold_value) ? update_post_meta($post_id, '_property_sold_value', $sold_value) : delete_post_meta($post_id, '_property_sold_value');

    }

    function state_picker($name, $selected){ ?>
        <select name="<?php echo $name; ?>" id="<?php echo $name; ?>">
        <?php $states = array(
            'AL' => 'Alabama',
            'AK' => 'Alaska',
            'AZ' => 'Arizona',
            'AR' => 'Arkansas',
            'CA' => 'California',
            'CO' => 'Colorado',
            'CT' => 'Connecticut',
            'DE' => 'Delaware',
            'DC' => 'District Of Columbia',
            'FL' => 'Florida',
            'GA' => 'Georgia',
            'HI' => 'Hawaii',
            'ID' => 'Idaho',
            'IL' => 'Illinois',
            'IN' => 'Indiana',
            'IA' => 'Iowa',
            'KS' => 'Kansas',
            'KY' => 'Kentucky',
            'LA' => 'Louisiana',
            'ME' => 'Maine',
            'MD' => 'Maryland',
            'MA' => 'Massachusetts',
            'MI' => 'Michigan',
            'MN' => 'Minnesota',
            'MS' => 'Mississippi',
            'MO' => 'Missouri',
            'MT' => 'Montana',
            'NE' => 'Nebraska',
            'NV' => 'Nevada',
            'NH' => 'New Hampshire',
            'NJ' => 'New Jersey',
            'NM' => 'New Mexico',
            'NY' => 'New York',
            'NC' => 'North Carolina',
            'ND' => 'North Dakota',
            'OH' => 'Ohio',
            'OK' => 'Oklahoma',
            'OR' => 'Oregon',
            'PA' => 'Pennsylvania',
            'RI' => 'Rhode Island',
            'SC' => 'South Carolina',
            'SD' => 'South Dakota',
            'TN' => 'Tennessee',
            'TX' => 'Texas',
            'UT' => 'Utah',
            'VT' => 'Vermont',
            'VA' => 'Virginia',
            'WA' => 'Washington',
            'WV' => 'West Virginia',
            'WI' => 'Wisconsin',
            'WY' => 'Wyoming'
        );
        foreach($states as $state =>$name ){
            $sel = ($selected == $state) ? ' selected="selected"' : '' ;
            printf('<option value="%s"%s>%s</option>', $state, $sel, $name);
        }
        ?>
        </select><?php
    }
}
