if (typeof auctioneer !== "undefined") {
    jQuery(document).ready(function($){
        $("select#post_status").append("<option value='" + auctioneer.statusValue + "'" + auctioneer.statusSelected + ">" + auctioneer.statusText + "</option>");
        $(".misc-pub-section label").append(" <b>" + auctioneer.statusLabel + "</b>");
        $("#post_status").on('change', function(){
            $(".misc-pub-section label b").empty();
        });
    });
    jQuery(document).ready(function($){
        $('select[name="_status"] option[value="' + auctioneer.statusValue + '"]').remove();
        $('select[name="_status"]').append('<option value="' + auctioneer.statusValue + '">' + auctioneer.statusText + '</option>');
    });
}

