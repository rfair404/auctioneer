<?php
namespace Auctioneer;

class Activation
{
    function __construct($plugin){
        add_action('init', array($this, 'do_activation'), 20);
        add_action('init', array($this, 'flush_rewrites'), 20);
        add_action('init', array($this, 'create_sample_content'), 30);
    }

    function do_activation(){
        update_option('auctioneer_activation_complete', true);
    }

    function flush_rewrites(){
       flush_rewrite_rules();
    }

    function create_sample_content(){
        $terms = $this->get_default_terms();
        if(is_array($terms)){
            foreach ($terms as $taxonomy => $terms){
                foreach($terms as $term){
                    if(!term_exists($term, $taxonomy)){
                         wp_insert_term($term, $taxonomy);
                    }
                }
            }
        }
    }

    function get_default_terms(){
        return array(
            'auctioneer_type' => array('Live', 'Off-site', 'On-site', 'Online', 'Webcast'),
            'auctioneer_category' => array('Personal Property', 'Business Property', 'Equipment or Industrial', 'Charity or Benefit', 'Urban Land or Lots', 'Rural Land', 'Timber Property', 'Residential Real Estate', 'Commercial or Industrial Real Estate', 'Online Auctions', 'Computers or Technology', 'Vehicles or Rolling Stock', 'Aeronautical', 'Marine', 'Bankruptcy'),
            // 'auctioneer_zoning' => array('Commercial', 'Residential', 'Lots & Land', 'Business Liquidation', 'Equiptment & Personal Property', 'Fundraising & Other'),
        );
    }
}
