<?php
namespace Auctioneer;
/**
 * The Auction Contact Widget
 * @todo refactor this to work with new p2p style user associations
 */
class AuctionContact extends \WP_Widget
{
    function __construct(){
        $widget_ops = array( 'classname' => 'auction-contact', 'description' => __('Auction Manager Info', 'auctioneer') );
        $control_ops = array( 'width' => 200, 'height' => 250, 'id_base' => 'auction-contact' );
        $this->WP_Widget( 'auction-contact', __('Auctions Manager Info', 'auctioneer'), $widget_ops, $control_ops );
    }

    function widget($args, $instance){

        if(!is_singular() || !in_array( get_post_type(), array('auctioneer_auction', 'auctioneer_property')) ){
            return;
        }

        extract($args);

        echo $before_widget;

        $title = apply_filters( 'widget_title', empty( $instance['title'] ) ? '' : $instance['title'], $instance, $this->id_base);
        if($title){
            echo $before_title .  $title . $after_title;
        }

        $sales_contact = get_post_meta(get_the_id(), '_auction_contact', true);
        //if this isn't set, it will get the post author as fallback.
        $sales_contact_data = ($sales_contact) ? get_userdata($sales_contact) : get_userdata( get_the_author_meta('ID') );
        $sales_contact_meta = ($sales_contact) ? get_user_meta($sales_contact) : get_user_meta( get_the_author_meta('ID') );

        if($sales_contact_data && $sales_contact_meta){
            //prevent issues if for some reason it isn't there
            ?>
            <ul>
            <?php
                printf('<li class="contact-text">%s <span class="name">%s</span></li>', __('For more information about this auction contact'), $sales_contact_data->display_name);//stub this should go in widget options
                printf('<li class="contact-phone">%s <span class="phone-number">%s</span></li>', __('Phone:', 'auctioneer'), apply_filters('auctioneer-phone-format', $sales_contact_meta['phone_number'][0]));
                printf('<li class="contact-email">%s <span class="email">%s</span></li>', __('Email:', 'auctioneer'), apply_filters('auctioneer-email-format', $sales_contact_data->user_email));
            ?>
            </ul>
            <?php
        }

        echo $after_widget;
    }

    function update($new_instance, $old_instance) {
        return $new_instance;
    }

    function form($instance) {
        $instance = wp_parse_args((array)$instance, array(
            'title' => '',
        ));

        $instance['title'] = (!empty($instance['title'])) ? $instance['title'] : '' ; ?>
        <p><label for="<?php echo $this->get_field_id('Title'); ?>"><?php _e('Title', 'auctioneer'); ?>:</label>
        <input type="text" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" value="<?php echo esc_attr( $instance['title'] ); ?>" size="25" /><br />
        <span class="howto" style="clear:both;"><?php _e('Enter the widget title as you wish it to appear', 'auctioneer'); ?></span></p>

    <?php
    }
}
