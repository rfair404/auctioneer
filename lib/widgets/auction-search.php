<?php
namespace Auctioneer;
/**
 * The Auction Search Widget
 */
class AuctionSearch extends \WP_Widget
{
    function __construct(){
        $widget_ops = array( 'classname' => 'auction-search', 'description' => __('Auction Search', 'auctioneer') );
        $control_ops = array( 'width' => 200, 'height' => 250, 'id_base' => 'auction-search' );
        $this->WP_Widget( 'auction-search', __('Auctions Search', 'auctioneer'), $widget_ops, $control_ops );
    }

    function widget($args, $instance){

        extract($args);

        echo $before_widget;

        $title = apply_filters( 'widget_title', empty( $instance['title'] ) ? '' : $instance['title'], $instance, $this->id_base);
        if($title){
            echo $before_title .  $title . $after_title;
        } ?>

        <div class="auction-search-results"></div>
        <?php echo $after_widget;
    }

    function update($new_instance, $old_instance) {
        return $new_instance;
    }

    function form($instance) {
        $instance = wp_parse_args((array)$instance, array(
            'title' => '',
        ));

        $instance['title'] = (!empty($instance['title'])) ? $instance['title'] : '' ; ?>
        <p><label for="<?php echo $this->get_field_id('Title'); ?>"><?php _e('Title', 'auctioneer'); ?>:</label>
        <input type="text" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" value="<?php echo esc_attr( $instance['title'] ); ?>" size="25" /><br />
        <span class="howto" style="clear:both;"><?php _e('Enter the widget title as you wish it to appear', 'auctioneer'); ?></span></p>

    <?php
    }
}
