<?php
namespace Auctioneer;
use Auctioneer\Common;

class DocumentList extends \WP_Widget
{
    function __construct(){
        $widget_ops = array( 'classname' => 'documents-list', 'description' => __('Auction Document List', 'auctioneer') );
        $control_ops = array( 'width' => 200, 'height' => 250, 'id_base' => 'documents-list' );
        $this->WP_Widget( 'documents-list', __('Document List', 'auctioneer'), $widget_ops, $control_ops );
    }

    function widget($args, $instance){

        if(!is_singular()){
            return;
        }
        extract($args);
        $docs = new \WP_Query( array(
            'connected_type' => 'auctioneer_auction_to_document',
            'connected_items' => get_queried_object(),
            'nopaging' => true,
        ) );
        if ( $docs->have_posts() ) :
        echo $before_widget;
        $title = apply_filters( 'widget_title', empty( $instance['title'] ) ? '' : $instance['title'], $instance, $this->id_base);
        if($title){
            echo $before_title .  $title . $after_title;
        } ?>
        <ul>
            <?php while ( $docs->have_posts() ) : $docs->the_post();
            $title = (p2p_get_meta( get_post()->p2p_id, 'title', true )) ? p2p_get_meta( get_post()->p2p_id, 'title', true ) : get_the_title();
            printf('<li><a href="%s" target="_blank" title="%s">%s</a></li>', esc_url(get_permalink()), esc_attr($title), $title );
            endwhile; ?>
        </ul>
        <?php
            // Prevent weirdness
            wp_reset_postdata();
        endif;
        echo $after_widget;
    }

    function update($new_instance, $old_instance) {
        return $new_instance;
    }

    function form($instance) {
        $instance = wp_parse_args((array)$instance, array(
            'title' => '',
        ));
        $instance['title'] = (!empty($instance['title'])) ? $instance['title'] : '' ; ?>
        <p><label for="<?php echo $this->get_field_id('Title'); ?>"><?php _e('Title', 'auctioneer'); ?>:</label>
        <input type="text" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" value="<?php echo esc_attr( $instance['title'] ); ?>" size="25" /><br />
        <span class="howto" style="clear:both;"><?php _e('Enter the widget title as you wish it to appear', 'auctioneer'); ?></span></p>

    <?php
    }
}
