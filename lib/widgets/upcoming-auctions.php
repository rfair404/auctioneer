<?php

namespace Auctioneer;
use Auctioneer\Common;

/**
 * The Recent Auction Widget
 */
class UpcomingAuctions extends \WP_Widget
{
    function __construct(){
        $widget_ops = array( 'classname' => 'upcoming-auctions', 'description' => __('Upcoming Auctions', 'auctioneer') );
        $control_ops = array( 'width' => 200, 'height' => 250, 'id_base' => 'upcoming-auctions' );
        $this->WP_Widget( 'upcoming-auctions', __('Upcoming Auctions', 'auctioneer'), $widget_ops, $control_ops );
    }

    function widget($args, $instance){
        extract($args);

        $query_args = array(
            'post_type' => 'auctioneer_auction',
            'posts_per_page'     => $instance['number'],
        );
        switch($instance['sort']){
            case 'publication_date':
                $orderby = array(
                    'orderby' => 'date',
                    'order' => 'DESC'
                );
            break;
            case 'auction_start_date':
                $orderby = array(
                    'orderby' => 'meta_value',
                    'order' => 'ASC',
                    'meta_key' => '_auction_start',
                    'meta_query' => array(
                        array(
                            'key' => '_auction_start',
                            'value' => array( time(), time()+60*60*24*7*365),
                            'compare' => 'BETWEEN',
                            'type' => 'NUMERIC',
                        ),
                    ),
                );
            break;
            case 'auction_end_date':
                $orderby = array(
                    'orderby' => 'meta_value',
                    'order' => 'ASC',
                    'meta_key' => '_auction_end',
                    'meta_query' => array(
                        array(
                            'key' => '_auction_end',
                            'value' => array( time(),  time()+60*60*24*7*365),
                            'compare' => 'BETWEEN',
                            'type' => 'NUMERIC',
                        ),
                    ),
                );
            break;
        }
        $query_args = wp_parse_args($orderby, $query_args);
        $auctions = new \WP_Query($query_args);
        if($auctions->have_posts()){
            echo $before_widget;
            $title = apply_filters( 'widget_title', empty( $instance['title'] ) ? '' : $instance['title'], $instance, $this->id_base);
            if($title){
                echo $before_title .  $title . $after_title;
            }
            while( $auctions->have_posts() ){
                $auctions->the_post();
                ?><div <?php post_class(); ?>>
                <?php printf('<h4 class="item-title"><a href="%s" title="%s">%s</a></h4>', get_permalink(), the_title_attribute( array( 'echo' => false ) ), get_the_title()); ?>
                <?php if(has_post_thumbnail()){
                    printf('<a href="%s" title="%s" class="posts-image">', get_permalink(), the_title_attribute( array('echo' => false ) ) );
                    echo '<figure class="featured-image">';
                    the_post_thumbnail('thumbnail');
                    echo '</figure>';
                    echo "</a>";
                }
                the_excerpt();
                echo '</div>';
            }
            echo $after_widget;
        }
        wp_reset_query();
    }

    function update($new_instance, $old_instance) {
        return $new_instance;
    }

    function form($instance) {
        $instance = wp_parse_args((array)$instance, array(
            'title' => '',
            'number' => false,
            'sort'  => ''
        ));
        $instance['title'] = (!empty($instance['title'])) ? $instance['title'] : '' ; ?>
        <p><label for="<?php echo $this->get_field_id('Title'); ?>"><?php _e('Title', 'auctioneer'); ?>:</label>
        <input type="text" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" value="<?php echo esc_attr( $instance['title'] ); ?>" size="25" /><br />
        <span class="howto" style="clear:both;"><?php _e('Enter the widget title as you wish it to appear', 'auctioneer'); ?></span></p>
        <?php $instance['number'] = (!empty($instance['number'])) ? $instance['number'] : '' ; ?>
        <p><label for="<?php echo $this->get_field_id('Number'); ?>"><?php _e('Number', 'auctioneer'); ?>:</label>
        <input type="number" id="<?php echo $this->get_field_id('number'); ?>" name="<?php echo $this->get_field_name('number'); ?>" value="<?php echo esc_attr( $instance['number'] ); ?>" size="5" /><br />
        <span class="howto" style="clear:both;"><?php _e('Select the number of auctions to display', 'auctioneer'); ?></span></p>
        <?php $instance['sort'] = (!empty($instance['sort'])) ? $instance['sort'] : '' ; ?>
        <p><label for="<?php echo $this->get_field_id('sort'); ?>"><?php _e('Sort by', 'auctioneer'); ?>:</label>
        <select id="<?php echo $this->get_field_id('sort'); ?>" name="<?php echo $this->get_field_name('sort'); ?>">
            <?php echo esc_attr( $instance['sort'] ); ?>
            <option value="publication_date" <?php if($instance['sort'] == 'publication_date') { echo 'selected'; } ?>><?php _e('Publication date', 'auctioneer'); ?></option>
            <option value="auction_start_date" <?php if($instance['sort'] == 'auction_start_date') { echo 'selected'; } ?>><?php _e('Auction start date', 'auctioneer'); ?></option>
            <option value="auction_end_date" <?php if($instance['sort'] == 'auction_end_date') { echo 'selected'; } ?>><?php _e('Auction end date', 'auctioneer'); ?></option>
        </select>
        <span class="howto" style="clear:both;"><?php _e('Choose how to sort the auction listings', 'auctioneer'); ?></span></p>
    <?php
    }

}
