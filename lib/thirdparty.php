<?php
namespace Auctioneer;
use Auctioneer\Common;


class Thirdparty{
    function __construct(Common $common){
        $this->common = $common;
        add_action('plugins_loaded', array($this, 'load_thirdpartys'), 10);
        add_action( 'tgmpa_register', array($this, 'register_required_plugins'), 10 );
    }

    function load_thirdpartys(){
        require_once('thirdparty/tgm-plugin-activation/class-tgm-plugin-activation.php');
    }

    function register_required_plugins(){
        $plugins = array(
            array(
                'name' => 'WP Document Revisions',
                'slug' => 'wp-document-revisions',
                'required' => true,
                'force_activation' => true,
            ),
            array(
                'name' => 'Posts to Posts',
                'slug' => 'posts-to-posts',
                'required' => true,
                'force_activation' => true,
            ),
            array(
                'name' => 'Query Multiple Taxonomies',
                'slug' => 'query-multiple-taxonomies',
                'required' => false,
                'force_activation' => false
            ),
            array(
                'name' => 'Gravityforms',
                'slug' => 'gravityforms',
                'required' => true,
                'version' => '1.8.5',
                'source' => $this->common->lib_dir . '/thirdparty/bundled/gravityforms_1.8.5.zip',
            ),
        );

        $config = array(
            'domain' => 'auctioneer',
            'parent_menu_slug' => 'options-general.php',
            'parent_url_slug' => 'options-general.php',
            'menu' => 'install-required-plugins',
            'has_notices' => true,
            'message' => '',
            'strings'           => array(
                'page_title'                                => __( 'Install Required Plugins', 'auctioneer' ),
                'menu_title'                                => __( 'Install Plugins', 'auctioneer' ),
                'installing'                                => __( 'Installing Plugin: %s', 'auctioneer' ), // %1$s = plugin name
                'oops'                                      => __( 'Something went wrong with the plugin API.', 'auctioneer' ),
                'notice_can_install_required'               => _n_noop( 'Auctioneer requires the following plugin: %1$s.', 'Auctioneer requires the following plugins: %1$s.' ), // %1$s = plugin name(s)
                'notice_can_install_recommended'            => _n_noop( 'Auctioneer recommends the following plugin: %1$s.', 'Auctioneer recommends the following plugins: %1$s.' ), // %1$s = plugin name(s)
                'notice_cannot_install'                     => _n_noop( 'Sorry, but you do not have the correct permissions to install the %s plugin. Contact the administrator of this site for help on getting the plugin installed.', 'Sorry, but you do not have the correct permissions to install the %s plugins. Contact the administrator of this site for help on getting the plugins installed.' ), // %1$s = plugin name(s)
                'notice_can_activate_required'              => _n_noop( 'The following required plugin is currently inactive: %1$s.', 'The following required plugins are currently inactive: %1$s.' ), // %1$s = plugin name(s)
                'notice_can_activate_recommended'           => _n_noop( 'The following recommended plugin is currently inactive: %1$s.', 'The following recommended plugins are currently inactive: %1$s.' ), // %1$s = plugin name(s)
                'notice_cannot_activate'                    => _n_noop( 'Sorry, but you do not have the correct permissions to activate the %s plugin. Contact the administrator of this site for help on getting the plugin activated.', 'Sorry, but you do not have the correct permissions to activate the %s plugins. Contact the administrator of this site for help on getting the plugins activated.' ), // %1$s = plugin name(s)
                'notice_ask_to_update'                      => _n_noop( 'The following plugin needs to be updated to its latest version to ensure maximum compatibility with this theme: %1$s.', 'The following plugins need to be updated to their latest version to ensure maximum compatibility with this theme: %1$s.' ), // %1$s = plugin name(s)
                'notice_cannot_update'                      => _n_noop( 'Sorry, but you do not have the correct permissions to update the %s plugin. Contact the administrator of this site for help on getting the plugin updated.', 'Sorry, but you do not have the correct permissions to update the %s plugins. Contact the administrator of this site for help on getting the plugins updated.' ), // %1$s = plugin name(s)
                'install_link'                              => _n_noop( 'Begin installing plugin', 'Begin installing plugins' ),
                'activate_link'                             => _n_noop( 'Activate installed plugin', 'Activate installed plugins' ),
                'return'                                    => __( 'Return to Required Plugins Installer', 'auctioneer' ),
                'plugin_activated'                          => __( 'Plugin activated successfully.', 'auctioneer' ),
                'complete'                                  => __( 'All plugins installed and activated successfully. %s', 'auctioneer' ) // %1$s = dashboard link
            )
        );

        tgmpa( $plugins, $config );
    }
}
