'use strict';
module.exports = function(grunt) {

    grunt.initConfig({
        jshint: {
              options: {
                    jshintrc: '.jshintrc'
              },
              all: [
                    'Gruntfile.js',
                    'lib/admin/js/*.js',
                    'lib/display/js/*.js',
                    '!lib/admin/js/*.min.js'
              ]
        },
        uglify: {
            my_target: {
                  files: {
                    'lib/admin/js/auctioneer-misc.min.js': ['lib/admin/js/auctioneer-misc.js']
                  }
                }
        },
        less: {
            dist: {
                options: {
                    compile: true,
                    compress: true
                },
                files: {
                    'lib/admin/css/auctioneer-edit.min.css': [
                        'lib/admin/less/auctioneer-edit.less'
                    ],
                     'lib/admin/css/auctioneer-settings.min.css': [
                        'lib/admin/less/auctioneer-settings.less'
                    ],
                    'lib/display/css/auctioneer.min.css': [
                        'lib/display/less/auctioneer.less'
                    ]
                }
            }
        },
        watch: {
            less: {
                files: [
                    'lib/admin/less/*.less',
                    'lib/display/less/*.less',
                ],
                tasks: ['less']
            },
            js : {
                files: [
                    'lib/admin/js/*.js',
                    '!lib/admin/js/*.min.js',
                    'lib/display/js/*.js',
                    'lib/display/js/*.min.js'
                ]
            },
            other: {
                files: ['lib/admin/admin.php']
            },
            options: {
                livereload: 1337,
            }
        }
    });

    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-jshint');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-less');

    grunt.registerTask('dev', ['watch']);
    grunt.registerTask('compile', ['jshint', 'uglify', 'less']);
};
