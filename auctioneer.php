<?php

/*
 * Plugin Name: Auctioneer
 * Description: Handles auction listings for AuctionEbid.com
 * Author: Russell Fair
 * Author URI: http://q21.co
 * Version: 0.0.1
 */

namespace Auctioneer;
use Auctioneer\Core;
use Auctioneer\Activation;

class Auctioneer{
    function __construct(){

        //load remaining plugin functionaligy
        require_once('lib/auctioneer-core.php');
        new Core();

        if(!get_option('auctioneer_activation_complete')){
            require_once('lib/activation.php');
            new Activation();
        }
    }



}
new Auctioneer;


